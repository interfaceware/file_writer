require "FIL.FILreadWrite"

-- return a unique filename (YYYYMMDD_guid.txt)
local function generateFilename()
   local FileName = os.date("%Y%m%d_")
   FileName = FileName .. util.guid(128):sub(-5) .. ".txt"

   while os.fs.stat(FileName) do
      FileName = FileName .. util.guid(128):sub(-5) .. ".txt"
   end
   return FileName
end

-- The main function is called whenever a message is
-- received by this component
function main(Data)
   local Configs = component.fields()
   -- Construct the file path
   local FileName = generateFilename()
   
   -- Create the directory if it does not exist
   if not os.fs.access(Configs.Destination) then
      os.fs.mkdir{path=Configs.Destination, parents=true}
      iguana.logWarning("WARNING: Created dir "..Configs.Destination);
   end
   
   local FilePath = Configs.Destination .. FileName
   iguana.logDebug("Saving data to file: " .. FilePath.." ("..#Data.." bytes)")
   -- Write Data to file
   if not iguana.isTest() then
      FILwrite(FilePath, Data)
   end
end

iguana.log("Output to: " .. component.fields().Destination)

